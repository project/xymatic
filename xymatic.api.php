<?php

/**
 * @file
 * API documentation for the xymatic module.
 */

use Drupal\media\MediaInterface;

/**
 * Allows other modules to react to Xymatic webhook.
 *
 * @param \Drupal\media\MediaInterface $media
 *   The created, updated or will be deleted media item.
 * @param array $requestBody
 *   The request body of the Xymatic webhook.
 */
function hook_xymatic_webhook(MediaInterface $media, array $requestBody) {
}

/**
 * Allows other modules to set up project-specific metadata attributes.
 *
 * @code
 * <?php
 *
 * function example_xymatic_metadata_info() {
 *   $metadata['credit'] = [
 *     'label' => t('Video credit'),
 *     'jsonPath' => 'userMetadata.credit',
 *   ];
 *   return $metadata;
 * }
 * @endcode
 */
function hook_xymatic_metadata_info() {
}

/**
 * Allows other modules to alter project-specific metadata values.
 *
 * @param mixed $result
 *   The value of the metadata attribute.
 * @param array $json
 *   The JSON object of the Xymatic webhook.
 * @param string $name
 *   The name of the metadata attribute.
 */
function hook_xymatic_metadata_alter(&$result, array $json, $name) {
}
